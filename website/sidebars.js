module.exports = {
/** sidebar for neva */
  nevaSidebar: [
        {
      type: 'category',
      label: 'Neva Documentation',
      items: [
        {type: 'doc', id: 'neva',},
        {type: 'doc', id: 'neva/translate',},
      ],
    },
  ],
/** sidebar for toro */

};
